# Barcode Reader #

This app simply allows the user to press a button, point the camera at a barcode (or a QR code), and get the number or text that was encoded. It should be possible to use this within any other app.

With the two `.java` files (located in `/app/src/main/java/com/google/zxing/integration/android/`) in place, you need only add the following two code snippets to get the scans.

1) Import Statements
-------------------
```java
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
```

2) Activate the scanner
-----------------------
```java
/* For example, place in the onClick method for your Scan button. */
IntentIntegrator scanIntegrator = new IntentIntegrator(this);
scanIntegrator.initiateScan(); // On the first attempt, the user will
                               // be directed to the Google Play Store 
                               // to install Barcode Scanner if needed.
```

3) Process the data that is scanned
-----------------------------------
```java
public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
    if (scanningResult != null) {
        /* 
         * Initialize these two strings above the constructor(s) 
         * to simply store results and access them from another method 
         */
        String scanContent = scanningResult.getContents();
        String scanFormat = scanningResult.getFormatName();

        // Example use of data: Display on screen
        formatTxt.setText("FORMAT: " + scanFormat);
        contentTxt.setText("CONTENT: " + scanContent);
    }
    else{
        Toast toast = Toast.makeText(getApplicationContext(),
                "No scan data received!", Toast.LENGTH_SHORT);
        toast.show();
    }
}
```

### Source ###

* [YouTube Tutorial](https://www.youtube.com/watch?v=Tb7gmrm6z7U)
* [Written Tutorial with Code](http://android-coffee.com/tutorial-how-to-create-barcode-reader-app-in-android-studio-1-4/)

### Testing ###
* Compiled with Android Studio 1.3 (minSdkVersion 9, targetSdkVersion 23)
* Works on Moto G (1st gen) with Android 5.1